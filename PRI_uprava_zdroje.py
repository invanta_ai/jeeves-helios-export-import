# 1. CAST: Smaže řádky s kódem "7", druhý a poslední zbytečný řádek, přidá tri placeholder sloupce a pak doplni hodnoty

#Importy
from openpyxl.utils.cell import rows_from_range
import pandas as pd
import numpy as np
import openpyxl as xl

report = "purchase_source.xlsx"
wb_report = xl.load_workbook(report)
ws_report = wb_report.worksheets[0]

# určení počtu sloupců a řádků
mr = ws_report.max_row

print("Na zacatku: ", mr)

ws_report.delete_rows(2)

mr = ws_report.max_row
mc = ws_report.max_column

# Doplnění zdrojoveho souboru o dva "placeholder" sloupce: "customer ID" a "Invoice line" 

#doplneni customer ID do noveho sloupce
for i in range(2, mr+1):
    ws_report.cell(row=i, column=8).value = 'ID'

#doplneni Invoice Line do noveho sloupce
for i in range(2, mr+1):
    ws_report.cell(row=i, column=9).value = 'Dodavatel'

#doplneni Invoice Line do noveho sloupce
for i in range(2, mr+1):
    ws_report.cell(row=i, column=10).value = 'ICO'


print ("Pred odmazanim sedmicek:", mr)
#smazani radku, ktere patri do line 7
i = 1
while i <= mr:
    if ws_report.cell(i,1).value == '7':
        ws_report.delete_rows(i)
    else:
        i += 1

wb_report.save(str(report))

report = "purchase_source.xlsx"
partneri = "PARTNERI.xlsx"

# pojmenovani openpyxl
wb_report = xl.load_workbook(report)
ws_report = wb_report.worksheets[0]

df_report = pd.read_excel(report)
df_partneri = pd.read_excel(partneri)

partneri_wb = xl.load_workbook(partneri)
report_wb = xl.load_workbook(report)

ws_partneri = partneri_wb.worksheets[0]
ws_report = wb_report.worksheets[0]

rb = ws_partneri.max_row
mr = ws_report.max_row

print ("Po znovunahrani:", mr)

# porovnani a doplneni cisla zakaznika
for i in range(0, mr-1):
    for x in range (1,rb-1):
        if df_report.iloc[i,6] == df_partneri.iloc[x,8]:
            df_report.iloc[i,7] = df_partneri.iloc[x,0]
            df_report.iloc[i,8] = df_partneri.iloc[x,1]
            df_report.iloc[i,9] = df_partneri.iloc[x,7]
            break

# nahrání do výsledného souboru
df_report.to_excel(report, index=False)
# Vyplnění dokumentu VYDPOL

#Importy
from openpyxl.utils.cell import rows_from_range
import pandas as pd
import numpy as np
import openpyxl as xl

report = "sales_source.xlsx"
VYDPOL = "VYDPOL.xlsx"
partneri = "PARTNERI.xlsx"

# pojmenovani openpyxl
wb_report = xl.load_workbook(report)
wb_VYDPOL = xl.load_workbook(VYDPOL)

ws_report = wb_report.worksheets[0]
ws_VYDPOL = wb_VYDPOL.worksheets[0]

df_report = pd.read_excel(report)
df_partneri = pd.read_excel(partneri)

partneri_wb = xl.load_workbook(partneri)
report_wb = xl.load_workbook(report)

ws_partneri = partneri_wb.worksheets[0]
ws_report = wb_report.worksheets[0]
ws_VYDPOL = wb_VYDPOL.worksheets[0]

# určení počtu sloupců a řádků
rb = ws_partneri.max_row
mr = ws_report.max_row

# zkopírování základní částky (sloupec D), dane a jejich couctu z reportu do VYDPOL.xlsx
for i in range(2, mr+1):
    x = ws_report.cell(row=i, column=3)
    y = ws_report.cell(row=i, column=4)
    ws_VYDPOL.cell(row=i, column=4).value = x.value
    ws_VYDPOL.cell(row=i, column=7).value = y.value
    ws_VYDPOL.cell(row=i, column=5).value = x.value + y.value
    ws_VYDPOL.cell(row=i, column=8).value = x.value + y.value
    if x.value == 0:
        ws_VYDPOL.cell(row=i, column=6).value = x.value * 100
        ws_VYDPOL.cell(i,6).number_format = '0'
    else:
        ws_VYDPOL.cell(row=i, column=6).value = y.value / x.value * 100
        ws_VYDPOL.cell(i,6).number_format = '0'

# zkopírování čísla VAT (sloupec A) z reportu do P_TYP v VYDPOL.xlsx
for i in range(2, mr+1):
    x = ws_report.cell(row=i, column=1)
    ws_VYDPOL.cell(row=i, column=2).value = x.value

# Doplneni P_TEXT v VYDPOL.xlsx
for i in range(2, mr+1):
    x = ws_report.cell(row=i, column=2)
    ws_VYDPOL.cell(row=i, column=3).value = x.value

# ukládání části openpyxl
wb_VYDPOL.save(str(VYDPOL)) 

# 5. ČÁST

# doplneni cisla radku faktury do polozky

vydfak = "VYDFAK.xlsx"
VYDPOL = "VYDPOL.xlsx"

df_vydfak = pd.read_excel(vydfak)
df_VYDPOL = pd.read_excel(VYDPOL)

prifak_wb = xl.load_workbook(vydfak)
VYDPOL_wb = xl.load_workbook(VYDPOL)

ws_vydfak = prifak_wb.worksheets[0]
ws_VYDPOL = VYDPOL_wb.worksheets[0]

rf = ws_vydfak.max_row
rp = ws_VYDPOL.max_row

# změna čísel v P_TYP
cisla = [
    (df_VYDPOL['P_TYP'] == 1),
    (df_VYDPOL['P_TYP'] == 20),
    (df_VYDPOL['P_TYP'] == 21), 
]
cislatyp = [100, 2001, 2100]

df_VYDPOL['P_TYP'] = np.select(cisla, cislatyp)


# porovnani a doplneni cisla zakaznika
for i in range(0, rp-1):
    for x in range (0, rf-1):
        if df_VYDPOL.iloc[i,2] == df_vydfak.iloc[x,3]:
            df_VYDPOL.iloc[i,0] = df_vydfak.iloc[x,0]
            break

# nahrání do výsledného souboru
df_VYDPOL.to_excel(VYDPOL, index=False)

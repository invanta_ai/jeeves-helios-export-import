# Vyplnění dokumentu PRIPOL


#Importy
from openpyxl.utils.cell import rows_from_range
import pandas as pd
import numpy as np
import openpyxl as xl

report = "purchase_source.xlsx"
pripol = "PRIPOL.xlsx"
partneri = "PARTNERI.xlsx"

# pojmenovani openpyxl
wb_report = xl.load_workbook(report)
wb_pripol = xl.load_workbook(pripol)

ws_report = wb_report.worksheets[0]
ws_pripol = wb_pripol.worksheets[0]

df_report = pd.read_excel(report)
df_partneri = pd.read_excel(partneri)

partneri_wb = xl.load_workbook(partneri)
report_wb = xl.load_workbook(report)

ws_partneri = partneri_wb.worksheets[0]
ws_report = wb_report.worksheets[0]
ws_pripol = wb_pripol.worksheets[0]

# určení počtu sloupců a řádků
rb = ws_partneri.max_row
mr = ws_report.max_row

# zkopírování základní částky (sloupec D) z reportu do P_ZAKLAD v PRIPOL.xlsx
for i in range(2, mr+1):
    x = ws_report.cell(row=i, column=4)
    y = ws_report.cell(row=i, column=5)
    ws_pripol.cell(row=i, column=4).value = x.value
    ws_pripol.cell(row=i, column=7).value = y.value
    ws_pripol.cell(row=i, column=5).value = x.value + y.value
    ws_pripol.cell(row=i, column=8).value = x.value + y.value
    if x.value == 0:
        ws_pripol.cell(row=i, column=6).value = x.value * 100
        ws_pripol.cell(i,6).number_format = '0'
    else:
        ws_pripol.cell(row=i, column=6).value = y.value / x.value * 100
        ws_pripol.cell(i,6).number_format = '0.'

# zkopírování čísla VAT (sloupec A) z reportu do P_TYP v PRIPOL.xlsx
for i in range(2, mr+1):
    x = ws_report.cell(row=i, column=1)
    ws_pripol.cell(row=i, column=2).value = x.value

# Doplneni P_TEXT v PRIPOL.xlsx
for i in range(2, mr+1):
    x = ws_report.cell(row=i, column=2)
    ws_pripol.cell(row=i, column=3).value = x.value

# ukládání části openpyxl
wb_pripol.save(str(pripol)) 

# 5. ČÁST

# doplneni cisla radku faktury do polozky

prifak = "PRIFAK.xlsx"
pripol = "PRIPOL.xlsx"

df_prifak = pd.read_excel(prifak)
df_pripol = pd.read_excel(pripol)

prifak_wb = xl.load_workbook(prifak)
pripol_wb = xl.load_workbook(pripol)

ws_prifak = prifak_wb.worksheets[0]
ws_pripol = pripol_wb.worksheets[0]

rf = ws_prifak.max_row
rp = ws_pripol.max_row

# změna čísel v P_TYP
cisla = [
    (df_pripol['P_TYP'] == 3),
    (df_pripol['P_TYP'] == 5),
    (df_pripol['P_TYP'] == 40), 
]
cislatyp = [4301, 4303, 4001]

df_pripol['P_TYP'] = np.select(cisla, cislatyp)


# porovnani a doplneni cisla zakaznika
for i in range(0, rp-1):
    for x in range (0,rf-1):
        if df_pripol.iloc[i,2] == df_prifak.iloc[x,4]:
            df_pripol.iloc[i,0] = df_prifak.iloc[x,0]
            break

# nahrání do výsledného souboru
df_pripol.to_excel(pripol, index=False)
# Secteni polozek na fakture, aby hodnota hlavicky sedela s polozkama, zapsani VYDFAK a VYDPOL jako .csv

#Importy
from openpyxl.utils.cell import rows_from_range
import pandas as pd
import numpy as np
import openpyxl as xl

VYDfak = "VYDFAK.xlsx"
VYDpol = "VYDPOL.xlsx"

VYDfak_wb = xl.load_workbook(VYDfak)
VYDpol_wb = xl.load_workbook(VYDpol)

ws_VYDfak = VYDfak_wb.worksheets[0]
ws_VYDpol = VYDpol_wb.worksheets[0]

rf = ws_VYDfak.max_row
rp = ws_VYDpol.max_row

# vynulovani castek na fakturach 

for i in range (2, rf + 1):
    ws_VYDfak.cell(row=i, column=7).value = 0

VYDfak_wb.save(str(VYDfak))

# secitani polozek

for i in range (2, rp + 1):
    a = ws_VYDpol.cell(row=i, column=1).value + 1
    ws_VYDfak.cell(row=a, column=7).value = ws_VYDfak.cell(row=a, column=7).value + ws_VYDpol.cell(row=i, column=5).value

# nahrání do výsledného souboru
VYDfak_wb.save(str(VYDfak))

VYDfak = "VYDFAK.xlsx"
VYDpol = "VYDPOL.xlsx"

VYDfakcsv = pd.read_excel (VYDfak)
VYDfakcsv.to_csv ('VYDFAK.csv', index = None, header=True)

VYDpolcsv = pd.read_excel (VYDfak)
VYDpolcsv.to_csv ('VYDPOL.csv', index = None, header=True)
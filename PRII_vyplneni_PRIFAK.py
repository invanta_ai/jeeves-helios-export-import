#Vyplní soubor PRIFAK, smaže "duplicity" (pro faktury s více záznamy nechá jen jeden řádek jako hlavičku), očísluje řádky

#Importy
from openpyxl.utils.cell import rows_from_range
import pandas as pd
import numpy as np
import openpyxl as xl
from openpyxl.styles import NamedStyle

# 3. ČÁST
report = "purchase_source.xlsx"
prifak = "PRIFAK.xlsx"

# práce s openpyxl
wb_report = xl.load_workbook(report)
wb_prifak = xl.load_workbook(prifak)

ws_report = wb_report.worksheets[0]
ws_prifak = wb_prifak.worksheets[0]

# určení počtu sloupců a řádků
mr = ws_report.max_row
mc = ws_report.max_column
print(mr)
print(mc)

# zkopírování dodavatelskeho cisla faktury (sloupec B) z reportu do DOKLAD ve priFAK.xlsx
for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=3)
    ws_prifak.cell(row=i, column=4).value = c.value

# ICO (sloupec J) z reportu do Fir_ICO ve PRIFAK.xlsx
for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=10)
    ws_prifak.cell(row=i, column=19).value = c.value


for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=2)
    ws_prifak.cell(row=i, column=5).value = c.value
# zkopírování interniho cisla faktury (sloupec B) z reportu do DOKLAD2 ve priFAK.xlsx

for i in range(2, mr+1):
    for j in range(2, 4):
        c = ws_report.cell(row=i, column=6)
        ws_prifak.cell(row=i, column=j).value = c.value
# zkopírování data (sloupec E) z reportu do DATUM a DATUM2 ve priFAK.xlsx

for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=8)
    ws_prifak.cell(row=i, column=6).value = c.value
# cislo -> cis

for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=9)
    ws_prifak.cell(row=i, column=7).value = c.value
# nazev -> text

for i in range(2, mr+1):
    for j in range(10, 16):
        ws_prifak.cell(row=i, column=j).value = 0
# doplnění nul do všech sloupců, kde je jedno, co tam bude

for i in range(2, mr+1):
    for j in range(17, 19):
        ws_prifak.cell(row=i, column=j).value = 0
# doplnění nul do všech sloupců, kde je jedno, co tam bude


for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=4)
    d = ws_report.cell(row=i, column=5)
    ws_prifak.cell(row=i, column=8).value = c.value + d.value
# doplnění KCS (součtem základní částky a daně)

for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=6)
    ws_prifak.cell(row=i, column=9).value = c.value
# doplneni data do DATSPL ve priFAK

for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=7)
    ws_prifak.cell(row=i, column=16).value = c.value
# přidání dič do priFAK (pouzijeme sloupec ktery neni k nicemu)

wb_prifak.save(str(prifak))

# Odstraneni duplicit z PRIFAK

prifak = "PRIFAK.xlsx"

df_prifak = pd.read_excel(prifak)

df_prifak = df_prifak.drop_duplicates(subset='DOKLAD2', keep='first')

df_prifak.to_excel(prifak, index=False)

# Ocislovani radku v PRIFAK

prifak = "PRIFAK.xlsx"
wb_prifak = xl.load_workbook(prifak)
ws_prifak = wb_prifak.worksheets[0]

mr = ws_prifak.max_row

# čísla od 1 do počtu řádků v CISLO v priFAK.xlsx
for i in range(2, mr+1):
    c = ws_prifak.cell(row=i, column=1)
    ws_prifak.cell(row=i, column=1).value = (i + 1) - 2

# Nastaveni formatu datumu

date_style = NamedStyle(name='date', number_format='DD/MM/YYYY')

for i in range(2, mr+1):
    ws_prifak.cell(row=i, column=2).style = date_style
    ws_prifak.cell(row=i, column=3).style = date_style
    ws_prifak.cell(row=i, column=9).style = date_style

wb_prifak.save(str(prifak))
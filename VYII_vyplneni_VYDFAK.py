#Vyplní soubor VYDFAK, smaže "duplicity" (pro faktury s více záznamy nechá jen jeden řádek jako hlavičku), očísluje řádky

#Importy
from openpyxl.utils.cell import rows_from_range
import pandas as pd
import numpy as np
import openpyxl as xl
from openpyxl.styles import NamedStyle

# 3. ČÁST
report = "sales_source.xlsx"
vydfak = "VYDFAK.xlsx"

# práce s openpyxl
wb_report = xl.load_workbook(report)
wb_vydfak = xl.load_workbook(vydfak)

ws_report = wb_report.worksheets[0]
ws_vydfak = wb_vydfak.worksheets[0]

# určení počtu sloupců a řádků
mr = ws_report.max_row
mc = ws_report.max_column
print(mr)
print(mc)

# ICO (sloupec I) z reportu do ODB_ICO ve VYDFAK.xlsx
for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=9)
    ws_vydfak.cell(row=i, column=15).value = c.value


# zkopírování interniho cisla faktury (sloupec B) z reportu do DOKLAD ve VYDFAK.xlsx
for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=2)
    ws_vydfak.cell(row=i, column=4).value = c.value

# zkopírování data (sloupec E) z reportu do DATUM a DATUM2 ve VYDFAK.xlsx
for i in range(2, mr+1):
    for j in range(2, 4):
        c = ws_report.cell(row=i, column=5)
        ws_vydfak.cell(row=i, column=j).value = c.value

# cislo -> cis
for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=7)
    ws_vydfak.cell(row=i, column=5).value = c.value

# nazev -> text
for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=8)
    ws_vydfak.cell(row=i, column=6).value = c.value

# doplnění nul do všech sloupců, kde je jedno, co tam bude
for i in range(2, mr+1):
    for j in range(8, 14):
        ws_vydfak.cell(row=i, column=j).value = 0

# doplneni 0 pro DPH ke kodum 20 a 21 a nulovym fakturam
for i in range(2, mr+1):
    if ws_report.cell(row=i, column=1).value == 20:
        ws_report.cell(row=i, column=4).value = 0        
    if ws_report.cell(row=i, column=1).value == 21:
        ws_report.cell(row=i, column=4).value = 0 
    if ws_report.cell(row=i, column=3).value == 0:
        ws_report.cell(row=i, column=4).value = 0 

# doplnění KCS (součtem základní částky a daně)
for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=3)
    d = ws_report.cell(row=i, column=4)
    ws_vydfak.cell(row=i, column=7).value = c.value + d.value

# doplneni data do DATSPL VYDFAK
for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=5)
    ws_vydfak.cell(row=i, column=14).value = c.value

# přidání dič do priFAK (pouzijeme sloupec ktery neni k nicemu)
for i in range(2, mr+1):
    c = ws_report.cell(row=i, column=6)
    ws_vydfak.cell(row=i, column=16).value = c.value

# Zmena formatu bunek pro sloupce s datumy na (DD.MM.YYYY)

wb_vydfak.save(str(vydfak))

# Odstraneni duplicit z VYDFAK

vydfak = "VYDFAK.xlsx"

df_vydfak = pd.read_excel(vydfak)

df_vydfak = df_vydfak.drop_duplicates(subset='DOKLAD', keep='first')

df_vydfak.to_excel(vydfak, index=False)

# Ocislovani radku v VYDFAK

vydfak = "VYDFAK.xlsx"
wb_vydfak = xl.load_workbook(vydfak)
ws_vydfak = wb_vydfak.worksheets[0]

mr = ws_vydfak.max_row

for i in range(2, mr+1):
    ws_vydfak.cell(row=i, column=1).value = (i - 1)

# Nastaveni formatu datumu

date_style = NamedStyle(name='date', number_format='DD/MM/YYYY')

for i in range(2, mr+1):
    ws_vydfak.cell(row=i, column=2).style = date_style
    ws_vydfak.cell(row=i, column=3).style = date_style
    ws_vydfak.cell(row=i, column=14).style = date_style

wb_vydfak.save(str(vydfak))

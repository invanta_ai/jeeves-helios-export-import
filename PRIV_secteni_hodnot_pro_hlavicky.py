# Secteni polozek na fakture, aby hodnota hlavicky sedela s polozkama, zapsani PRIFAK a PRIPOL jako .csv

#Importy
from openpyxl.utils.cell import rows_from_range
import pandas as pd
import numpy as np
import openpyxl as xl

prifak = "PRIFAK.xlsx"
pripol = "PRIPOL.xlsx"

prifak_wb = xl.load_workbook(prifak)
pripol_wb = xl.load_workbook(pripol)

ws_prifak = prifak_wb.worksheets[0]
ws_pripol = pripol_wb.worksheets[0]

rf = ws_prifak.max_row
rp = ws_pripol.max_row

# vynulovani castek na fakturach 

for i in range (2, rf + 1):
    ws_prifak.cell(row=i, column=8).value = 0

prifak_wb.save(str(prifak))

# secitani polozek

for i in range (2, rp + 1):
    a = ws_pripol.cell(row=i, column=1).value + 1
    ws_prifak.cell(row=a, column=8).value = ws_prifak.cell(row=a, column=8).value + ws_pripol.cell(row=i, column=5).value

# nahrání do výsledného souboru
prifak_wb.save(str(prifak))

prifak = "PRIFAK.xlsx"
pripol = "PRIPOL.xlsx"

prifakcsv = pd.read_excel (prifak)
prifakcsv.to_csv ('PRIFAK.csv', index = None, header=True)

pripolcsv = pd.read_excel (prifak)
pripolcsv.to_csv ('PRIPOL.csv', index = None, header=True)